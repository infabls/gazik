<!-- <link rel="stylesheet" href="/css/libs.min.css">
-->
<link rel="stylesheet" href="scss/bt.css">
<link rel="stylesheet" href="/base.css">
<link rel="stylesheet" href="/head.css">
<link rel="manifest" href="/manifest.json"></head>
<?php require'section/data_header.php';?>
<body data-spy="scroll">
<div class="wrapper">
	<?php// require'section/data.php';?>
	<!-- ============старая кнопка============== -->
	<!-- <div>
	<button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target="#myNavmenu" data-canvas="body">
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
</div>  -->
<!-- ============старая кнопка============== -->



<div id="mobile-fixed-menu" >
	<div class="dropdown" style="    position: absolute;
    top: 6px;
    left: 20px;">
<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> <i class="fa fa-globe" aria-hidden="true"></i>
	Выберите регион
	<span class="caret"></span>
</button>
<ul class="dropdown-menu" style="left: 0px">
	<li>
		<a href="/astana?city">Акм. обл и Астана</a>
	</li>
	<li>
		<a href="/karaganda?city">Кар. обл и Караганда</a>
	</li>
	<li>
		<a href="/pavlodar?city">Павл. обл и Павлодар</a>
	</li>
	<li>
		<a href="/ust-kamenogorsk?city">ВКО и Усть-Каменогорск</a>
	</li>
	<li class="divider"></li>
	<li>
		<a href="/">Нет моего города</a>
	</li>
</ul>
</div>
</div>
	<button type="button" class="navbar-toggle" onclick="show()">
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>


</div>
</div>

<script >
				
	function show() {
			document.getElementById('myNavmenu').style.display='block'
		}

		function hide() {
			document.getElementById('myNavmenu').style.display='none'
			// body...
		}



</script>
<header>

<div class="row  hidden-xs">
<div class="topper">
	<div class="col-md-2 col-sm-2 col-xs-2">
		<a href="index">
			<div class="topper__logo"></div>
		</a>

	</div>
	<div class="col-md-4 col-sm-3 col-xs-4">
		<div class="dropdown" style="  margin-left: 40px;  padding: 8px 0;">
			<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> <i class="fa fa-globe" aria-hidden="true"></i>
				Выберите регион
				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu">
				<li>
					<a href="/astana?city=0" >Акмолинская обл. и Астана</a>
				</li>
				<li>
					<a href="/karaganda?city=1">Карагандинская обл. и Караганда</a>
				</li>
				<li>
					<a href="/pavlodar?city=2">Павлодарская обл. и Павлодар</a>
				</li>
				<li>
					<a href="/ust-kamenogorsk?city=3">ВКО обл. и Усть-Каменогорск</a>
				</li>
				<li class="divider"></li>
				<li>
					<a href="/">Нет моего города</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="col-md-6  col-sm-7 col-xs-6">
		<div class="topper__contact">
			<div class="topper__phone-icon"></div>
			<p> 
				<strong><a href="tel:<?=$nomber1?>"><?=$nomber1?></a>
					<br>
					<a href="tel:<?=$nomber2?>"><?=$nomber2?></a></strong> 
			</p>
		</div>
		<div class="topper__contact">
			<div class="topper__address-icon"></div>
			<div class="topper_addr"><p><strong><?=$addr?></strong></p></div>
			
		</div>
	</div>
</div>

</div>
</div>

<nav id="myNavmenu" class="navmenu navmenu-default navmenu-fixed-left offcanvas" role="navigation">

<button type="button" style="top: 55px !important; padding: 7px 13px;font-size: 20px;" class="navbar-toggle" onclick="hide()">
	
<i class="fa fa-times" aria-hidden="true"></i>
</button>
<ul class="nav navmenu-nav">
<li class="active">
<a href="index?city">Главная</a>
</li>
<li>
<a href="avtogaz?city">Газовые заправки</a>
</li>
<li>
<a href="ustanovka-gbo?city">Газобалонное оборудование</a>
</li>
<li>
<a href="dostavka-gaza?city">Доставка газа</a>
</li>
<li>
<a href="gazgolder?city">Газ для газгольдеров</a>
</li>
<li>
<a href="/faq">Ответы на ваши вопросы</a>
</li>
<li class="hidden-sm hidden-md">
<a href="/price">Цены</a>
</li>
<li>
<a href="contacts?city">Контакты</a>
</li>
</ul>
</nav>
<nav id="top_nav" class="navbar hidden-xs" >
<div id="navbar" class="navbar-collapse menu-collapse menu-shadow collapse in" aria-expanded="true">
<ul class="nav navbar-nav">
<li class="active">
	<a href="index?city=<?=$_GET['city']?>">Главная</a>
</li>
<li class=" dropdown-toggle" type="button" data-toggle="dropdown">
	<a href="#">
		Услуги
		<span class="caret"></span>
	</a>
</li>
<ul class="dropdown-menu">
	<li>
		<a href="dostavka-gaza?city=<?=$_GET['city']?>">Доставка газа в баллонах</a>
	</li>
	<li>
		<a href="ustanovka-gbo?city=<?=$_GET['city']?>">Установка ГБО на авто</a>
	</li>
	<li>
		<a href="avtogaz?city=<?=$_GET['city']?>">Газовые заправки</a>
	</li>
	<li>
		<a href="gazgolder?city=<?=$_GET['city']?>">Сжиженный газ для газгольдеров</a>
	</li>
	<li class="divider"></li>
	<li>
		<a href="uslugi?city=<?=$_GET['city']?>">Все услуги</a>
	</li>
</ul>
<li>
	<a href="avtogaz?city=<?=$_GET['city']?>">АЗГС</a>
</li>
<li>
	<a href="ustanovka-gbo?city=<?=$_GET['city']?>">ГБО</a>
</li>
<li>
	<a href="dostavka-gaza?city=<?=$_GET['city']?>">Доставка газа</a>
</li>
<li>
	<a href="/price?city=<?=$_GET['city']?>">Цены</a>
</li>
<li>
	<a href="/faq?city=<?=$_GET['city']?>">Вопросы</a>
</li>
<li class="hidden-sm hidden-md">
	<a href="/about?city=<?=$_GET['city']?>">О компании</a>
</li>
<li>
	<a href="/contacts?city=<?=$_GET['city']?>">Контакты</a>
</li>
</ul>
</div>
</nav>
</header>
<main>
<script src="js/jquery.js"></script>
<!-- <script  
  src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
  integrity="sha256-k2WSCIexGzOj3Euiig+TlR8gA0EmPjuc79OEeY5L45g="
  crossorigin="anonymous"></script>
-->
