</main>
<footer>
	<style>
	footer h2 {font-size: 30px; margin-bottom: 10px; margin-top: 20px;}
	footer a {    font-size: 19px;
    margin-bottom: 5px;
    display: inline-block;}
	.totop {
    color: white;
    top: -15px;
    margin-left: -15px;
    position: absolute;
    background-color: rgba(39, 255, 96, 0.66);
    border-radius: 50%;
    padding: 9px;
    box-shadow: 0px 0px 7px 1px rgba(0, 0, 0, 0.35);
}
footer {
	text-align: center;
	width: 100%; 
	background-color: #2d2d2d; 
	min-height: 200px;}
.smm {
    width: 20%;
    float: left;
}
.smm img {border-radius: 3px}
footer h2,footer h3 {color: #FFF}
.hr {display: inline-block;
    width: 80%;
    color: #821515;
    border-color: #0da2a2;
border-bottom-width: 2px;}

.bg-s {width: 48px; height: 48px; border-radius: 3px; }
.bg-fb {background: url('/img/social/css_sprites.png') -5px -5px;}
.bg-gp {background: url('/img/social/css_sprites.png') -63px -5px;}
.bg-in {background: url('/img/social/css_sprites.png') -121px -5px;}
.bg-ok {background: url('/img/social/css_sprites.png') -179px -5px;}
.bg-vk {background: url('/img/social/css_sprites.png') -237px -5px;}
</style>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-12 col-xs-12">
				<h2>Клиентам</h2>
				<a href="uslugi">Услуги</a>
				<br>
				<a href="contacts">Контакты</a>
				<br>
				<a href="/faq">Вопросы и ответы</a>
				<br>
				<a href="price">Цены</a>
				<br>
				<a rel="nofollow external" target="_blank" href="https://blog.gorgaz2050.kz">Полезная информация</a>
				<br></div>
			<div class="col-md-4 col-sm-12 col-xs-12" style="text-align: -webkit-center;">
				
					<h2>Мы в Соц сетях</h2>
					<div class="smm"><a href="https://vk.com/gorgaz2050" target="_blank" rel="external nofollow"><div class="bg-s bg-vk"></div></a></div>
					<div class="smm"><a href="https://www.facebook.com/gorgaz2050" target="_blank" rel="external nofollow"><div class="bg-s bg-fb"></div></a></div>
					<div class="smm"><a href="https://www.instagram.com/gorgaz2050/" target="_blank" rel="external nofollow"><div class="bg-s bg-in"></div></a></div>
					<div class="smm"><a href="https://ok.ru/gorgaz2050" target="_blank" rel="external nofollow"><div class="bg-s bg-ok"></div></a></div>
					<div class="smm"><a href="https://plus.google.com/b/100849713321869367498/100849713321869367498" target="_blank" rel="external nofollow"><div class="bg-s bg-gp"></div></a></div>
						<a href="tel:+77071810218">
							Call-центр работает с 8:00 до 18:00 ежедневно +7 (707) 181 02 18</a>
							<br><a href="tel:+77762889919">Заказы по телефону +7 (776) 288 99 19</a>
			</div>
				
			
			<div class="col-md-4 col-sm-12 col-xs-12">
				<h2>Выберите город</h2>
				<a href="/astana">Астана</a>
				<br>
				<a href="/karaganda">Караганда</a>
				<br>
				<a href="/pavlodar">Павлодар</a>
				<br>
				<a href="/ust-kamenogorsk">Усть-Каменогорск</a>
				<br>
				</div>
		</div>
	</div>
	<hr class="hr">
	<div class="copy" style="    color: white;
    padding-bottom: 30px;
    font-size: 16px;">
		<div class="row">All rights reserved. ТОО "Горгаз2050.kz"</div> <div style="padding-top: 20px;"><a href="https://infabls.com" target="_blank">Разработка и поддержка сайта - Infabls.com</a></div>
	</div>
</footer>

<link rel="stylesheet" href="css/font-awesome.min.css">
<script src="js/bootstrap.min.js"></script>
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script async src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->



<!-- //////////////////////////////// -->
</div>

<!-- =========фикс шапки при скролле=========== -->
<script>
var h_hght = 90; // высота шапки
var h_mrg = 0;    // отступ когда шапка уже не видна
                 
$(function(){
 
    var elem = $('#top_nav');
    var top = $(this).scrollTop();
     
    if(top > h_hght){
        elem.css('top', h_mrg);
    }           
     
    $(window).scroll(function(){
        top = $(this).scrollTop();
         
        if (top+h_mrg < h_hght) {
            elem.css('top', (h_hght-top));
        } else {
            elem.css('top', h_mrg);
        }
    });
 
});
</script>
<!-- =========фикс шапки при скролле=========== -->
<!-- ============google-analytics============== -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-101055933-3', 'auto');
  ga('send', 'pageview');
</script>
<!-- ============google-analytics============== -->
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter46092129 = new Ya.Metrika({
                    id:46092129,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });
        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/46092129" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = '3hs0JtXnPj';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->
</body>
</html>