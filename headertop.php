<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta name="theme-color" content="#960605">

	<meta name="robots" content="index, follow">
	<meta property="og:image" content="https://gorgaz2050.kz/img/bg.jpg">
	<meta property="og:type" content="article"> 
	<script type='application/ld+json'> 
{
  "@context": "http://www.schema.org",
  "@type": "GasStation",
  "name": "Горгаз",
  "url": "https://gorgaz2050.kz",
  "logo": "https://gorgaz2050.kz/img/lg11.png",
  "image": "https://gorgaz2050.kz/img/bg.jpg",
  "description": "Газовая компания Горгаз предлагает услуги газообеспечения самому широкому спектру потребителей. Сеть газовых заправок, доставка газа в баллонах, заправка газгольдеров сжиженным газом - наш неполный спектр услуг.",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "Коктал 23",
    "addressLocality": "Астана",
    "addressRegion": "Акмолинская область",
    "addressCountry": "Kazakhstan"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": "51.20606668",
    "longitude": "71.34979734"
  },
  "hasMap": "https://www.google.kz/maps/place/%D0%93%D0%BE%D1%80%D0%B3%D0%B0%D0%B7/@51.2043765,71.3513349,18z/data=!4m5!3m4!1s0x0:0xce75a2fb986e1e6!8m2!3d51.2042379!4d71.3510849",
  "openingHours": "Mo, Tu, We, Th, Fr, Sa, Su 08:00-20:00",
  "contactPoint": {
    "@type": "ContactPoint",
    "contactType": "Mobile",
    "telephone": "+77071810218"
  }
}
 </script>